
tlcpmemcheat
=====================

A very simple cheat engine for GNU+Linux. Written in GNU C.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)


How to build
---------------------

Execute `make.bash`

How to use
----------------------

Execute `bin\tlcpmemcheat.out <PID>`

How this program works
----------------------

It first does open the text file `\proc\$PID\maps` and parse the memory regions addresses.  
It then does scan and patch these memory regions with `process_vm_readv` and `process_vm_writev`.

See http://man7.org/linux/man-pages/man2/process_vm_readv.2.html