/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015-2017 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdlib.h>
#include <assert.h>
#include "base.h"

#define DEFINE_ARRAYLIST_INITIALIZE(Type)				\
	static inline Error ArrayList##Type##_initialize		\
	(CP_ArrayList##Type dest, C_Length capacity)			\
	{								\
		assert(dest);						\
		assert(!dest->elements);				\
		dest->elements = malloc(sizeof(Type) * capacity);	\
		if (!dest->elements)					\
			return ERROR_MEMORY;				\
		dest->length = 0;					\
		dest->capacity = capacity;				\
		return ERROR_NONE;					\
	};

#define DEFINE_ARRAYLIST_LENGTH(Type)\
	static inline Length ArrayList##Type##_length(CPC_ArrayList##Type a) \
	{							      \
		assert(a);					      \
		return a->length;				      \
	}

#define DEFINE_ARRAYLIST_CAPACITY(Type)\
	static inline Length ArrayList##Type##_capacity(CPC_ArrayList##Type a) \
	{							      \
		assert(a);					      \
		return a->capacity;				      \
	}

#define DEFINE_ARRAYLIST_FINALIZE(Type) \
	static inline Void ArrayList##Type##_finalize(P_ArrayList##Type a)	\
	{								\
		assert(a);						\
		if (a->elements)					\
			free(a->elements);				\
		*a = (ArrayList##Type){					\
			.length = 0,					\
			.capacity = 0,					\
			.elements = NULL				\
		};							\
	}

#define DEFINE_ARRAYLIST_FINALIZE_CLEANUP(Type) \
	static inline Void ArrayList##Type##_finalizeCleanup(P_ArrayList##Type a) \
	{								\
		ArrayList##Type##_finalize(a);				\
	}

#define DEFINE_ARRAYLIST_TRIM_CAPACITY(Type)	\
	static inline Error ArrayList##Type##_trimCapacity(CP_ArrayList##Type a) \
	{								\
		assert(a);						\
		assert(  ArrayList##Type##_length(a)			\
		       < ArrayList##Type##_capacity(a));		\
		if (0 == ArrayList##Type##_length(a))			\
		{							\
			free(a->elements);				\
			a->elements = NULL;				\
			a->capacity = 0;				\
			return ERROR_NONE;				\
		}							\
		a->elements = realloc(a->elements,			\
				      sizeof(Type) *			\
				      ArrayList##Type##_length(a));	\
		if (!a->elements)					\
			return ERROR_MEMORY;				\
		a->capacity = ArrayList##Type##_length(a);		\
		return ERROR_NONE;					\
	}

#define DEFINE_ARRAYLIST_ENSURE_CAPACITY(Type)				\
	static inline Error ArrayList##Type##_ensureCapacity		\
	(CP_ArrayList##Type a, C_Length capacity)			\
	{								\
		assert(a);						\
		assert(ArrayList##Type##_length(a) < capacity);		\
		a->elements = realloc(a->elements,			\
				     sizeof(Type) * capacity);		\
		if (!a->elements)					\
			return ERROR_MEMORY;				\
		a->capacity = capacity;					\
		return ERROR_NONE;					\
	}

#define DEFINE_ARRAYLIST_SIZE(Type) \
	static inline Size ArrayList##Type##_size (CPC_ArrayList##Type a) \
	{								\
		assert(a);						\
		return sizeof(ArrayList##Type) +			\
			(ArrayList##Type##_capacity(a) * sizeof(Type));	\
	}

#define DEFINE_ARRAYLIST_ADD_LAST(Type)					\
	static inline Error ArrayList##Type##_addLast			\
	(CP_ArrayList##Type a, Type element)				\
	{								\
		assert(a);						\
		++(a->length);						\
		if (a->length > a->capacity)				\
		{							\
			C_Error e =					\
			 ArrayList##Type##_ensureCapacity(a, a->length * 2); \
			if (ERROR_NONE != e)				\
				return e;				\
		}							\
		a->elements[a->length - 1] = element;			\
		return ERROR_NONE;					\
	}

#define DEFINE_ARRAYLIST_REMOVE_LAST(Type)				\
	static inline Error ArrayList##Type##_removeLast		\
	(CP_ArrayList##Type a, Type * const element)			\
	{								\
		assert(a);						\
		--(a->length);						\
		if (element)						\
			*element = a->elements[a->length];		\
		if (a->capacity / 2 > a->length)			\
		{							\
			C_Error e = ArrayList##Type##_trimCapacity(a);	\
			if (ERROR_NONE != e)				\
				return e;				\
		}							\
		return ERROR_NONE;					\
	}

#define DEFINE_ARRAYLIST_AT(Type)				\
	static inline Type ArrayList##Type##_at			\
	(CPC_ArrayList##Type a, Index position)			\
	{							\
		assert(a);					\
		assert(position < ArrayList##Type##_length(a)); \
		return a->elements[position];			\
	}

#define DEFINE_ARRAYLIST_AT_POINTER(Type)			\
	static inline Type *ArrayList##Type##_atPointer		\
	(CP_ArrayList##Type a, Index position)			\
	{							\
		assert(a);					\
		assert(position < ArrayList##Type##_length(a)); \
		return &a->elements[position];			\
	}

#define DEFINE_ARRAYLIST_LAST(Type)					\
	static inline Type ArrayList##Type##_last			\
	(CP_ArrayList##Type a)						\
	{								\
		assert(a);						\
		assert(0 < ArrayList##Type##_length(a));		\
		return a->elements[ArrayList##Type##_length(a) - 1];	\
	}

#define DEFINE_ARRAYLIST_LAST_POINTER(Type)				\
	static inline Type *ArrayList##Type##_lastPointer		\
	(CP_ArrayList##Type a)						\
	{								\
		assert(a);						\
		assert(0 < ArrayList##Type##_length(a));		\
		return &a->elements[ArrayList##Type##_length(a) - 1];	\
	}

#define DEFINE_ARRAYLIST_CLEAR(Type)					\
	static inline Error ArrayList##Type##_clear(CP_ArrayList##Type a) \
	{								\
		assert(a);						\
		a->length = 0;						\
		a->capacity = 0;					\
		if (a->elements)					\
			free(a->elements);				\
		a->elements = NULL;					\
		return ERROR_NONE;					\
	}

#define DEFINE_ARRAYLIST(Type)				\
	typedef struct  {				\
		Length length;				\
		Length capacity;			\
		Type *elements;				\
	} ArrayList##Type;				\
	DEFINE_TYPEDEFS(ArrayList##Type);		\
	DEFINE_ARRAYLIST_INITIALIZE(Type);		\
	DEFINE_ARRAYLIST_LENGTH(Type);			\
	DEFINE_ARRAYLIST_FINALIZE(Type);		\
	DEFINE_ARRAYLIST_FINALIZE_CLEANUP(Type);	\
	DEFINE_ARRAYLIST_CAPACITY(Type);		\
	DEFINE_ARRAYLIST_TRIM_CAPACITY(Type);		\
	DEFINE_ARRAYLIST_ENSURE_CAPACITY(Type);		\
	DEFINE_ARRAYLIST_SIZE(Type);			\
	DEFINE_ARRAYLIST_ADD_LAST(Type);		\
	DEFINE_ARRAYLIST_REMOVE_LAST(Type);		\
	DEFINE_ARRAYLIST_AT(Type);			\
	DEFINE_ARRAYLIST_AT_POINTER(Type);		\
	DEFINE_ARRAYLIST_LAST(Type);			\
	DEFINE_ARRAYLIST_LAST_POINTER(Type);		\
	DEFINE_ARRAYLIST_CLEAR(Type);

DEFINE_ARRAYLIST(U8);
DEFINE_ARRAYLIST(U16);
DEFINE_ARRAYLIST(U32);
DEFINE_ARRAYLIST(U64);
DEFINE_ARRAYLIST(S8);
DEFINE_ARRAYLIST(S16);
DEFINE_ARRAYLIST(S32);
DEFINE_ARRAYLIST(S64);
DEFINE_ARRAYLIST(R32);
DEFINE_ARRAYLIST(R64);
DEFINE_ARRAYLIST(Size);
DEFINE_ARRAYLIST(Char);
DEFINE_ARRAYLIST(Boolean);
DEFINE_ARRAYLIST(P_Void);
DEFINE_ARRAYLIST(P_U8);
DEFINE_ARRAYLIST(P_U16);
DEFINE_ARRAYLIST(P_U32);
DEFINE_ARRAYLIST(P_U64);
DEFINE_ARRAYLIST(P_S8);
DEFINE_ARRAYLIST(P_S16);
DEFINE_ARRAYLIST(P_S32);
DEFINE_ARRAYLIST(P_S64);
DEFINE_ARRAYLIST(P_R32);
DEFINE_ARRAYLIST(P_R64);
DEFINE_ARRAYLIST(P_Size);
DEFINE_ARRAYLIST(P_Char);
DEFINE_ARRAYLIST(P_Boolean);

#define ArrayList_STATIC {.length = 0, .capacity = 0, .elements = NULL}
