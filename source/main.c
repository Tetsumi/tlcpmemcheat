/*
  tlcmemcheat
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2017 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#define _GNU_SOURCE
#include "base.h"
#include "arraylist.h"
#include <stdio.h>
#include <stdio_ext.h>
#include <assert.h>
#include <sys/uio.h>

typedef struct iovec Iovec;
DEFINE_TYPEDEFS(FILE);
DEFINE_TYPEDEFS(Iovec);

typedef pid_t PID;

DEFINE_TYPEDEFS(PID);

typedef struct
{
	Address address;
	Length length;
} MemoryMap;

DEFINE_TYPEDEFS(MemoryMap);
DEFINE_ARRAYLIST(MemoryMap);

typedef struct
{
	PID pid;
	ArrayListMemoryMap almm;
} Process;

DEFINE_TYPEDEFS(Process);

typedef union
{
	P_U8   u8;
	P_U16  u16;
	P_U32  u32;
	P_U64  u64;
	P_S8   s8;
	P_S16  s16;
	P_S32  s32;
	P_S64  s64;
	P_Void v;

} USP;

typedef union
{
	U8   u8;
	U16  u16;
	U32  u32;
	U64  u64;
	S8   s8;
	S16  s16;
	S32  s32;
	S64  s64;
} USV;

DEFINE_TYPEDEFS(USP);
DEFINE_TYPEDEFS(USV);
DEFINE_ARRAYLIST(USP);

typedef struct
{
	Size vSize;
	Boolean (*pcmp)(C_USP usp, C_USV value);
        Void (*write)(C_USP usp, C_USV value);
} Scan;

DEFINE_TYPEDEFS(Scan);

Boolean cmpu8  (C_USP usp, C_USV value) { return  *usp.u8 == value.u8;  }
Boolean cmpu16 (C_USP usp, C_USV value) { return *usp.u16 == value.u16; }
Boolean cmpu32 (C_USP usp, C_USV value) { return *usp.u32 == value.u32; }
Boolean cmpu64 (C_USP usp, C_USV value) { return *usp.u64 == value.u64; }
Boolean cmps8  (C_USP usp, C_USV value) { return  *usp.s8 == value.s8;  }
Boolean cmps16 (C_USP usp, C_USV value) { return *usp.s16 == value.s16; }
Boolean cmps32 (C_USP usp, C_USV value) { return *usp.s32 == value.s32; }
Boolean cmps64 (C_USP usp, C_USV value) { return *usp.s64 == value.s64; }

Void    writeu8  (C_USP usp, C_USV value) {  *usp.u8 = value.u8;  }
Void    writeu16 (C_USP usp, C_USV value) { *usp.u16 = value.u16; }
Void    writeu32 (C_USP usp, C_USV value) { *usp.u32 = value.u32; }
Void    writeu64 (C_USP usp, C_USV value) { *usp.u64 = value.u64; }
Void    writes8  (C_USP usp, C_USV value) {  *usp.s8 = value.s8;  }
Void    writes16 (C_USP usp, C_USV value) { *usp.s16 = value.s16; }
Void    writes32 (C_USP usp, C_USV value) { *usp.s32 = value.s32; }
Void    writes64 (C_USP usp, C_USV value) { *usp.s64 = value.s64; }

Error Scan_initialize (CP_Scan s, C_Size size, C_Boolean isSigned)
{
	assert(s);
	assert(0 < size);
	if (isSigned)
	{
		switch (size)
		{
		case 1:
			*s = (Scan){
				.vSize = 1,
				.pcmp = cmps8,
				.write = writes8
			};
			break;
		case 2:
			*s = (Scan){
				.vSize = 2,
				.pcmp = cmps16,
				.write = writes16
			};
			break;
		case 4:
			*s = (Scan){
				.vSize = 4,
				.pcmp = cmps32,
				.write = writes32
			};
			break;
		case 8:
			*s = (Scan){
				.vSize = 8,
				.pcmp = cmps64,
				.write = writes64
			};
			break;
		default:
			return ERROR_BAD_ARGUMENT;
		}
	}
	else
	{
		switch (size)
		{
		case 1:
			*s = (Scan){
				.vSize = 1,
				.pcmp = cmpu8,
				.write = writeu8
			};
			break;
		case 2:
			*s = (Scan){
				.vSize = 2,
				.pcmp = cmpu16,
				.write = writeu16
			};
			break;
		case 4:
			*s = (Scan){
				.vSize = 4,
				.pcmp = cmpu32,
				.write = writeu32
			};
			break;
		case 8:
			*s = (Scan){
				.vSize = 8,
				.pcmp = cmpu64,
				.write = writeu64
			};
			break;
		default:
			return ERROR_BAD_ARGUMENT;
		}
	}
	return ERROR_NONE;
}

Error Scan_finalize (CP_Scan s)
{
	assert(s);
	*s = (Scan){
		.vSize = 0,
		.pcmp = NULL,
		.write = NULL
	};
	return ERROR_NONE;
}

Error parseMaps (CP_ArrayListMemoryMap almm, CP_FILE mapsFiles)
{
	assert(almm);
	assert(mapsFiles);
	while(!feof(mapsFiles))
	{
	        P_Void start;
		P_Void end;
		Char   read;
		Char   write;
		Char   executable;
		Char   shared;
		fscanf(mapsFiles,
		       "%p-%p %c%c%c%c%*[^\n]\n",
		       &start,
		       &end,
		       &read,
		       &write,
		       &executable,
		       &shared);
		if ('r' != read || 'w' != write || 'p' != shared)
			continue;
		ArrayListMemoryMap_addLast(
			almm,
			(MemoryMap){
				.address = start,
			        .length = end - start
		        }
	        );
	}
	return ERROR_NONE;
}

Error Process_initialize (CP_Process p, C_PID pid)
{
	assert(p);
	Error e = ArrayListMemoryMap_initialize(&p->almm, 16);
	if (ERROR_NONE != e)
		return e;
	char s[30];
	sprintf(s, "/proc/%d/maps", pid);
	P_FILE mapsFile = fopen(s, "r");
	if (!mapsFile)
	{
		ArrayListMemoryMap_finalize(&p->almm);
		return ERROR_OPEN_FILE;
	}
	e = parseMaps(&p->almm, mapsFile);
	fclose(mapsFile);
	if (ERROR_NONE != e)
	{
		ArrayListMemoryMap_finalize(&p->almm);
		return e;
	}
	p->pid = pid;
	return ERROR_NONE;
}

Error Process_finalize (CP_Process p)
{
	assert(p);
        ArrayListMemoryMap_finalize(&p->almm);
	*p = (Process){
		.pid = 0,
	};
	return ERROR_NONE;
}

Error scanRegion (CPC_Iovec io,
		  CP_ArrayListUSP alp,
		  C_USV value,
		  CPC_Scan s,
		  P_Void base)
{
	assert(io);
	assert(alp);
	assert(s);
	assert(base);
        Length i = 0;
	while (i + s->vSize < io->iov_len)
	{
		USP p = {.v = io->iov_base + i};
		if (s->pcmp(p, value))
			ArrayListUSP_addLast(alp, (USP){.v = base + i});
		++i;
	}
	return ERROR_NONE;
}

Error scanRegions (CP_Process p,
		   CP_ArrayListUSP alpu,
		   C_USV value,
		   CPC_Scan s)
{
	assert(p);
	assert(alpu);
	assert(s);
	C_Size bSize = 1024 * 1024;
	P_Char *buffer = malloc(bSize);
	if (!buffer)
		return ERROR_MEMORY;
	Iovec iovRem;
	Iovec iovLoc = {
		.iov_base = buffer,
		.iov_len = bSize
	};
	for (Index i = 0; i < ArrayListMemoryMap_length(&p->almm); ++i)
	{	
		CPC_MemoryMap mm = ArrayListMemoryMap_atPointer(&p->almm, i);
		Index bi = 0;
		C_Index end = mm->length - s->vSize;
		while (bi < end)
		{
			iovRem.iov_base = mm->address + bi;
			C_Index diff = end - bi;
			iovRem.iov_len = diff < bSize ? diff : bSize;
			bi += iovRem.iov_len;
			iovLoc.iov_len = iovRem.iov_len;
			U32 re = process_vm_readv(p->pid,
						  &iovLoc,
						  1,
						  &iovRem,
						  1,
						  0);
			if (-1 == re)
			{
				free(buffer);
				return ERROR_SYSCALL;
			}
			Error e = scanRegion(&iovLoc,
					     alpu,
					     value,
					     s,
					     iovRem.iov_base);
			if (ERROR_NONE != e)
			{
				free(buffer);
				return e;
			}
		}
	}
	free(buffer);
	return ERROR_NONE;
}

Error filterScan (CPC_ArrayListUSP source,
		  CP_ArrayListUSP target,
		  CPC_Process p,
		  CPC_Scan s,
		  C_USV value)
	
{
	assert(source);
	assert(target);
	assert(p);
	assert(s);
	for (Index i = 0; i < ArrayListUSP_length(source); ++i)
	{
		USP vf = ArrayListUSP_at(source, i);
		USV vs = {.u64 = 0};
		Iovec iovRem = {
			.iov_base = vf.v,
			.iov_len = s->vSize
		};
		Iovec iovLoc = {
			.iov_base = &vs,
			.iov_len = s->vSize
		};
		int e = process_vm_readv(p->pid,
					 &iovLoc,
					 1,
					 &iovRem,
					 1,
					 0);
		if (-1 == e)
			return ERROR_SYSCALL;
		if (s->pcmp((USP){.v = &vs}, value))
			ArrayListUSP_addLast(target, vf);
	}
	return ERROR_NONE;
}

Error patchScan (CP_ArrayListUSP target,
		 CPC_Process p,
		 CPC_Scan s,
		 USV value)
{
	assert(target);
	assert(p);
	assert(s);
	for (Index i = 0; i < ArrayListUSP_length(target); ++i)
	{
		USP vt = ArrayListUSP_at(target, i);
		Iovec iovRem = {
			.iov_base = vt.v,
			.iov_len = s->vSize
		};
		Iovec iovLoc = {
			.iov_base = &value,
			.iov_len = s->vSize
		};
		int e = process_vm_writev(p->pid,
					  &iovLoc,
					  1,
					  &iovRem,
					  1,
					  0);
		if (-1 == e)
			return ERROR_SYSCALL;
	}
}

int main (int argc, char **argv)
{
	if (2 > argc)
	{
		P_ERROR("No process id provided.");
		return 0;
	}
	C_PID pid = atoi(argv[1]);	
	Process p = {.almm = ArrayList_STATIC};
	Error e = Process_initialize(&p, pid);
	if (ERROR_NONE != e)
	{
		P_ERROR("Coulnd't open the memory maps file -> ");
		perror("");
		return 0;
	}
	U32 sizeValue;
	Char typeValue;
	printf("Enter value byte size (1, 2, 4, or 8): ");
	scanf(" %u", &sizeValue);
	printf("Signed or unsigned? (s or u): ");
	scanf(" %c", &typeValue);
	C_Boolean isSigned = typeValue == 's';
	printf("Enter value to search: ");
	USV usv;
	scanf(isSigned? " %lld" : " %llu", &usv);
	Scan scan;
	Scan_initialize(&scan, sizeValue, isSigned);
	ArrayListUSP alu = ArrayList_STATIC;
	ArrayListUSP_initialize(&alu, 1000);
	e = scanRegions(&p, &alu, usv, &scan);
	if (ERROR_NONE != e)
	{
		P_ERROR("Scanning failed -> ");
		perror("");
		ArrayListUSP_finalize(&alu);
		Process_finalize(&p);
		return 0;
		
	}
	printf("Found %u values\n", ArrayListUSP_length(&alu));
	__fpurge(stdin);
	ArrayListUSP aluf = ArrayList_STATIC;
	ArrayListUSP_initialize(&aluf, 1000);
	P_ArrayListUSP a = &alu;
	P_ArrayListUSP b = &aluf;
	Char answer;
	do
	{
		printf("Press ENTER  when ready to refine the scan.");
		getchar();
		printf("Enter value to search: ");
		scanf(isSigned? "%lld" : "%llu", &usv);
		e = filterScan(a, b, &p, &scan, usv);
		if (ERROR_NONE != e)
		{
			P_ERROR("Filter failed -> ");
			perror("");
		}
		printf("Found %u values\n", ArrayListUSP_length(b));
		__fpurge(stdin);
		printf("Refine again? (y/n): ");
		scanf(" %c", &answer);
		__fpurge(stdin);
		a = (a == &alu) ? &aluf : &alu;
		b = (b == &alu) ? &aluf : &alu;
		ArrayListUSP_clear(b);
	} while ('y' == answer);
	printf("Enter new value: ");
	scanf(isSigned? "%lld" : "%llu", &usv);
	patchScan(a, &p, &scan, usv);
	ArrayListUSP_finalize(&aluf);
	ArrayListUSP_finalize(&alu);
	Process_finalize(&p);
	puts("Done!");
	return 0;
}
