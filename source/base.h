/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define DEFINE_TYPEDEFS(Type)				\
	typedef Type const C_## Type;			\
	typedef Type *P_## Type;			\
	typedef Type const *PC_## Type;			\
	typedef Type * const CP_## Type;		\
	typedef Type const * const CPC_## Type;		\
	typedef Type **PP_## Type;			\
	typedef Type const **PPC_## Type;		\
	typedef Type ** const CPP_## Type;		\
	typedef Type * const * PCP_## Type;		\
	typedef Type * const * const CPCP_## Type;	\
	typedef Type const * const * PCPC_## Type;	\
	typedef Type const ** const CPPC_## Type;	\
	typedef Type const * const * const CPCPC_## Type;

typedef   int8_t  S8;
typedef  int16_t S16;
typedef  int32_t S32;
typedef  int64_t S64;

typedef  uint8_t  U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;

typedef    float R32;
typedef   double R64;

typedef   size_t Size;
typedef   size_t Index;

typedef     char Char;
typedef     void Void;
typedef     bool Boolean;
typedef    void* Address;

DEFINE_TYPEDEFS(S8);
DEFINE_TYPEDEFS(S16);
DEFINE_TYPEDEFS(S32);
DEFINE_TYPEDEFS(S64);

DEFINE_TYPEDEFS(U8);
DEFINE_TYPEDEFS(U16);
DEFINE_TYPEDEFS(U32);
DEFINE_TYPEDEFS(U64);

DEFINE_TYPEDEFS(R32);
DEFINE_TYPEDEFS(R64);

DEFINE_TYPEDEFS(Size);
DEFINE_TYPEDEFS(Index);

DEFINE_TYPEDEFS(Char);
DEFINE_TYPEDEFS(Void);
DEFINE_TYPEDEFS(Boolean);
DEFINE_TYPEDEFS(Address);

typedef  S8  VectorS8 __attribute__ ((vector_size (16)));
typedef S16 VectorS16 __attribute__ ((vector_size (16)));
typedef S32 VectorS32 __attribute__ ((vector_size (16)));
typedef S64 VectorS64 __attribute__ ((vector_size (16)));

typedef  U8  VectorU8 __attribute__ ((vector_size (16)));
typedef U16 VectorU16 __attribute__ ((vector_size (16)));
typedef U32 VectorU32 __attribute__ ((vector_size (16)));
typedef U64 VectorU64 __attribute__ ((vector_size (16)));

typedef R32 VectorR32 __attribute__ ((vector_size (16)));
typedef R64 VectorR64 __attribute__ ((vector_size (16)));


DEFINE_TYPEDEFS(VectorS8);
DEFINE_TYPEDEFS(VectorS16);
DEFINE_TYPEDEFS(VectorS32);
DEFINE_TYPEDEFS(VectorS64);

DEFINE_TYPEDEFS(VectorU8);
DEFINE_TYPEDEFS(VectorU16);
DEFINE_TYPEDEFS(VectorU32);
DEFINE_TYPEDEFS(VectorU64);

DEFINE_TYPEDEFS(VectorR32);
DEFINE_TYPEDEFS(VectorR64);

typedef enum
{
	ERROR_NONE,
	ERROR_UNKNOWN,
	ERROR_MEMORY,
	ERROR_OPEN_FILE,
	ERROR_NOT_IMPLEMENTED,
	ERROR_BAD_ARGUMENT,
	ERROR_SYSCALL
} Error;

DEFINE_TYPEDEFS(Error);

typedef Size Length;

DEFINE_TYPEDEFS(Length);

#define CLEANUP(function) __attribute__ ((__cleanup__(function)))
#define P_ERROR(msg, ...) fprintf(stderr, "ERROR: "msg, ##__VA_ARGS__)
#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_REVISION 0
#define VERSION_STRING "1.0.0"
